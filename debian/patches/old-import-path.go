--- a/slice/pool.go
+++ b/slice/pool.go
@@ -8,7 +8,7 @@
 import (
 	"sync"
 
-	"modernc.org/mathutil"
+	"github.com/cznic/mathutil"
 )
 
 var (
--- a/file/file.go
+++ b/file/file.go
@@ -13,7 +13,7 @@
 
 	"modernc.org/fileutil"
 	"modernc.org/internal/buffer"
-	"modernc.org/mathutil"
+	"github.com/cznic/mathutil"
 	"github.com/edsrzf/mmap-go"
 )
 
